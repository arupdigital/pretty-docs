# Prettified Documentation

Create pretty pdfs.

Find the Markdown Cheatsheet [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) to help you create the Markdown files.

The final output will have the PrinceXML watermark in the top right hand corner of the first page. You *can* remove it if you open the pdf in Adobe Acrobat Reader or Bluebeam and delete the object.

## Requirements
* markdown-it (get using npm, use `--save` option)
* markdown-it-container (get using npm, use `--save` option)
* highlight.js (get using npm, use `--save` option)
* [PrinceXML](https://www.princexml.com/).

## Instructions

Update the path to the prince executable in `compile.js` by changing the `princePath` variable.

Change the `documentName` variable in `compile.js` to whatever you want your pdf to be called.

- `style.css` is for the overall style
- `layout.html` is for including other template-related things, where the only thing special is the `$content$` string, which is replaced with parsed markdown

The document will be created in the same directory.

Edit the markdown files only. The html is generated **from** the markdown.

The markdown files should all be in the doc folder. They are parsed in order of file name, hence `Chapter 1.md` comes before `Chapter 2.md` in the final pdf.

Run using `> node compile watch`