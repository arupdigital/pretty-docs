<div class="centered">
	<img src="images/Wicon.png" class="hero">
</div>

<header>
	<i class="fa fa-blind fa-2x" style="left: -10%; top: 25%;"></i>
	The Arup Digital Intern Survival Guide
	<small>by Nick Yu, Saloni Talati, Ajay Mistry, Taiki Maekawa</small>
	<small>Last Updated: August 30, 2019</small>
</header>

::: content

> A how-to guide and best practices for a successful and fulfilling internship.
:::

<div class="header-image-top" style="background-image: url(images/bridge.jpg)"></div>
<header style="margin-top: 10px;">
	Table of Contents
	<small></small>
</header>






<br/>

* Chapter 1: Welcome to Arup
* Chapter 2: Technical Tips for Consultants
* Chapter 3: Software Development


<div class="header-image" ></div>
<header>
	<i class="fa fa-compass fa-2x" style="left:93%; top: 25%;"></i>
	Chapter 1: Welcome to Arup
	<small>Background about the Digital team and the Toronto office</small>
</header>

::: content
Welcome to the Arup Digital team! You may be wondering why is it called Digital, and what is Digital? Your alternative title is ICT & Security Systems Designer, unless you're a Software Developer. The services the Digital consulting team offers can be split into two groups.
| Communications           | Security               |
| ------------------------ | ---------------------- |
| Structured cabling       | Video Surveillance     |
| Wi-Fi                    | Access Control         |
| A/V                      | Intrusion detection    |
| Intercom                 | Intercom               |
| Emergency call stations  | Duress                 |
| Public Address (PA)      | Emergency Call Station |
| LAN/WAN Active Equipment |                        |
| DAS                      |                        |
If you don't know how to use Revit or MicroStation, don't worry at all. There will be in-person tutorials to learn to the basics.

## Facilities

Coffee machines and dishwashers are located on each floor. Align your cup with the set of nozzles on the right side of the machine. The little dial on top of the coffee machine allows you to adjust the volume and amount of milk (depending on the drink). **DO NOT** leave dishes in the sink unless you want to experience Facilities' wrath. Printers are also located on each floor. The supply room for pens, notepads, etc. is located on the 9th floor to the right of the reception desk. Be on the lookout for emails every Thursday morning around 10 AM for the free morning snack!

## Timesheets

Be sure to get timesheet job numbers from your supervisor. Timesheets are due every Monday at noon. Finance will threaten to revoke email access if you don't submit your timesheet, but don't worry, this never happens. The Arup intranet will have links to submit your timesheet, request software from the software shop, and file a ticket to Facilities or the Service Desk.

# Arup tips
1. Dress business casual.
2. Ask questions. It doesn't get any simpler than this. If you are lacking the knowledge to get something done, ask questions before it gets too late. It's definitely better to ask early than to do something last minute.
3. Make your best attempt to find the answer to your question without interrupting the workflow of those around you. Everyone is busy and willing to help at Arup, but try to make their life easier, not just yours.
4. If you are done your work, ask for more. The work will come to you eventually, so avoid overtime by being extra-efficient during work hours.
5. Show a desire to constantly learn. Watch YouTube videos and tutorials in your free time for the software that you will be working with.
6. Write everything down. Everything your supervisors and colleagues say is useful and may help you later in the term. It’s better to ask for clarification to write something down properly than to repeat your question multiple times verbally and divert their attention. You should also write your questions and the answers to those questions down so that you don’t end up asking things again after not getting it the first time (which is okay).
7. Don’t be scared to talk to other disciplines if coordination is needed. Everyone at Arup is used to people coming and going and recognizes the tasks of interns.
8. Write down the tasks that you have. It helps you keep track of what needs to get done, and what has already been done. Especially if someone comes after you in a month or two asking if that one small task has been done and you don’t remember. (It will also MASSIVELY) help you when you plan out your intern presentation at the end of the term AND the work term report if you need to write one for this term).
9. Be aware that you should be coming in on time, and ready to leave late. Consulting hours are long, and you should brace yourself for when deliverable weeks come. 
10. Take advantage of all of the learning opportunities like Lunch and Learns and Fireside Chats. Free food plus knowledge!.
11. Don’t forget about the intern presentation. Keep it in your mind as you work on your day-to-day tasks so you have a good idea of what to include in your presentation. Be ready for questions in your intern presentation.
12. Take advantage of all of the resources around you.
13. Arup University is a great resource to learn more about Arup and the other disciplines.

<br/>
<br/>
<br/>
<br/>
<br/>

# Useful Contacts
- Electrical
	- [James Choi](mailto:Jaemin.Choi@arup.com)
	- [David Mitchell](mailto:David-F.Mitchell@arup.com)
	- [Cherilyn Ng](mailto:cherilyn.ng@arup.com)
	- [Tudor Munteanu](mailto:tudor.munteanu@arup.com)
- BIM
	- [Gavin Trevan](mailto:Gavin.Trevan@arup.com)
	- [Debora Machado](mailto:Debora.Machado@arup.com)
- IT
	- [Adrian Agao](mailto:adrian.agao@arup.com)
- Finance
	- [John Ferraro](mailto:John.Ferraro@arup.com)
- MicroStation
	- [Asmir Alicelebic](mailto:Asmir.Alicelebic@arup.com)
	- [Valeri Strong](mailto:Valeri.Strong@arup.com)
- Project Management
	- [Sherlita Di Bratto](mailto:Sherlita.Di-Bratto@arup.com)
	- [Marinette Malik](mailto:Marinette.Malik@arup.com)
- HR
	- [Abhishek Mishra](mailto:Abhishek.Mishra@arup.com)
	- [Ola Zmuda](mailto:Ola.Zmuda@arup.com)
- Digital Admin
	- [Tasneem Fatema](mailto:Tasneem.Fatema@arup.com)
- Civil
	- [Tobias Abrahams](mailto:Tobias.Abrahams@arup.com)
	- [Jeffrey Russell](mailto:Jeffrey.Russell@arup.com)
:::