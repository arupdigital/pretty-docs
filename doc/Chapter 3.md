<div class="header-image"></div>
<header>
    <i class="fa fa-code fa-2x" style="left:93%; top: 25%;"></i>
	Chapter 3: Software Development
	<small>Tips for development and Our Projects</small>
</header>

::: content

# Overview

Here is some tips and information about the projects past interns worked on.

[Andrew Phillips](mailto:Andrew.Phillips-G@arup.com) is the go-to guy for any help you might need over the term.
The MassMotion team can also help out too, so don't be afraid to ask them as well, although Andrew is definitely more helpful for web-related questions.

# Development Tips

## Getting Started

1. Check you have company account for different git services. (Bitbucket, Gitlab)
2. Go to repo and check the documentation for each project. (such as `readme.me`) What you need to know for each project is there.

## Development

1. If you are working in team, understanding git workflow is strong +.
2. Enjoy!

## Deployment

1. If you are going to deploy to AWS, use PuTTY
    - Frontend URL: ec2-user@ec2-35-182-40-20.ca-central-1.compute.amazonaws.com
    - Backend URL: ec2-user@ec2-35-182-63-90.ca-central-1.compute.amazonaws.com
    - Credentials: aws.ppk (this file should be in intern package)
    - Hosting directory: /usr/share/nginx/html
    - Projects directory: /usr/share/nginx/sites-available
    - Note: Two apps are currently running (EM and FOW) and PORT 3000 and 3001 used.

# Projects

Following are projects which past interns have worked on.

## In Development

### Interlinx

A project management tool for engineers currently on development.

Essentially, this is unifying a JIRA-like tool with the engineering specific tools required for Arup projects.

Repo: https://bitbucket.org/arupdigital/interlinx/src/master/

![Interlinx Architecture Diagram](images/InterlinxArchitecture.png)

### Emotional Mapping (EM)

A project to measure the stress level of the user through Empatica E4 wristband.

Web Frontend & Backend Repo: https://bitbucket.org/arupdigital/emotional-mapping-ui/src/master/

Android Frontend Repo: https://bitbucket.org/arupdigital/emotional-mapping-app/src/master/

Host (frontend): http://35.182.40.20/em/

Host (backend): http://35.182.63.90:3001/api

![Emotional Mapping Architecture Diagram](images/EMarchitecture.png)

PointCloud Model facts:
1. It's using potree to display the model
2. Given a lidar file (.las), use the Potree converter to 
   convert it into a potree format. Find the files in 
   emotional-mapping-ui/potree/pointclouds
3. To manipulate the toronto ofiice model refer to the file:
   emotional-mapping-ui/potree/examples/torontoOffice.html
4. To manipulate the map and bounding box refer to the file:
   emotional-mapping-ui/potree/src/viewer/map.js


Troubleshooting tips:

1. If the realtime data does not show up: 
    1. Make sure you are not on the Arup network
    2. Make sure to use the Backend host url on the android app
    3. Make sure you've clicked 'Start Uploading' on the app

2. Error "Failed to load module script": to solve this, go on the frontend server,
   file /usr/share/nginx/html/em, open index.html and make sure that base href is
   a relative path, not absolute.

3. If you stopped mongodb on the server and cannot restart it:
    1. Go to etc/yum.repos.d and clear cache:
        1. sudo rm -rf /var/cache/yum
        2. sudo yum clean all
    2. Erase the following files:
        1. sudo rm -rf /var/run/mongodb/mongo.pid
        2. sudo rm -rf /var/lib/mongo/mongod.lock
        3. sudo rm -rf /tmp/mongodb-27017.sock (not a typo, it's actually .sock)
    3. Give permissions: 
        1. sudo chmod a+x /var/lib/mongo
    4. Start the service:
        1. sudo service mongod start


## Maintaining

### FWLRT RM + IM

**FWLRT** = Finch West LRT (You'll see these 5 letters a lot).

**RM** = Requirement Management Tool

**IM** = Interface Management Tool

Our engineers use the RM and IM tools to manage their interfaces and respective requirements for the project.

Technically speaking, these tools are implemented in `PHP`, with a considerable amount of JavaScript going on as well.
We first run PSOS documents through a series of feeder scripts (written in `Python`) to 
break down the requirements and format them in a hierarchical spreadsheet, and then this spreadsheet is imported into the database.

RM (dev) Host: http://10.166.15.70/aruprm-dev/

IM (dev) Host: http://10.166.15.70/finch-dev/finch-im/

## Past Projects

### Digital Proposal Template

This tool allows our advisory team to dynamically create proposals using a wizard and content library. Written in `PHP`.

This project is in fairly good shape. Some changes may need to be made for formatting when the proposal is exported to a pdf.

### Prettified Documentation

A tool developed to generate **this** pdf.

Converts .md => .html => .pdf

Repo: https://gitlab.arup.com/tor_digital/prettified-documentation

### Future of Work (FOW)

A web app to keep record of brain storming ideas.

Repo: https://bitbucket.org/arupdigital/future-of-work/src/master/

Host: http://35.182.40.20/fow/
