<div class="header-image"></div>
<header>
	<!-- <i class="fa fa-copy fa-2x"></i> -->
	<i class="fa fa-pencil-square-o fa-2x" style="left:93%; top: 25%;"></i>
	Chapter 2: Technical Tips for Consultants
	<small>Revit, MicroStation, and more</small>
</header>

::: content
# Revit Tips
1. Whenever a drawing is issued, the drawing revision number is incremented. <div style="margin-left: 9%;"> ![](images/Capture1.png) </div>
2. When you are issuing a set of drawings, anything that has changed since the last time you issued a drawing is circled with a cloud and there is a triangle drawn connecting to the cloud. The inside of that triangle should have a number indicating what revision number that drawing is. <div style="margin-left: 28%;"> ![](images/Capture2.png) </div>
3. There is something called a Bluebeam Live Session where all of the drawings of all of the disciplines are uploaded onto a central working place, where disciplines can make markups about coordination issues (like a camera being on the same ceiling grid as a light)
4. When you address a markup, you need to mark it off as completed, and you need to reply to the markup with “DONE” to show everyone that the markup has indeed been addressed.<div style="margin-left: 6%;"> ![](images/Capture3.png) </div>
5. If you have a lot of markups to do for a single sheet, sometimes it is a good idea to print out the drawing, and mark off the changes as you make them with a highlighter. Then, go through a double check and mark off (with a different colour highlighter) the confirmed changes to make an “X” on each markup.
6. When placing a data outlet or a camera on the ceiling, make sure that it is in the centre of a ceiling tile <br/><div style="margin-left: 31%;"> ![](images/Capture4.png) </div>
7. When adding in, moving, or removing, or altering the location of a camera in any way, always remember that the associated data outlet for the camera must move with it.
8. Drawings aren’t as simple as dropping in devices where you need them. There things to consider like phasing and reference planes and hosts. Depending on where you mount a camera, and what you set the phase as, it can be visible in one sheet of the same physical space, but not be visible in another sheet. Make sure you set the properties of the device correctly to avoid headaches. <div style="margin-left: 28%;"> ![](images/Capture5.png) </div>
9. Always sync every few minutes. You never know when the model will crash on you. Don't forget to "Relinquish Elements" which is under Collaborate every time you finish syncing. <br/> <div style="margin-left: 14%;"> ![](images/Capture6.png) </div>
10. When opening a model on Revit, never double click on the model to open it. Always select it, ensure that the box for "Creating [a] New Local" file is checked off, and then click open.<div style="margin-left: 5%;"> ![](images/Capture.png) </div>
11. If you’re having Revit issues, ask the BIM team. [Gavin](mailto:Gavin.Trevan@arup.com) is incredibly knowledgeable and will get things done. [Debora](mailto:Debora.Machado@arup.com) is also really knowledgeable, but you should find out who on the BIM team is assigned to your project.
12. When asked to Batch Publish or print the drawings, go to the CADtools ribbon and then click Batch Publish. From the first dropdown menu, pick "DRAWING LIST" (security or comms, whichever set you are trying to batch publish). From the second dropdown menu, pick security or comms pdf depending on the set you picked from the first dropdown. Do not change anything under details. You can then Publish and see the set in the project folder.
13. When you’re working with 3D aspects of Revit, make sure that the elevations and directions of devices is right. Just because it looks fine on the 2D view, does not mean that it is acceptable. 
You need to submit 3D models to the contractors too, so they can come back with complaints if everything isn’t right. Making sure everything is right also includes the hosts and work planes because depending on these, the way in which the devices appear can be altered. If you find that something is not visible when it should be, try setting the detail level to fine rather than coarse. This is the rectangle at the bottom left corner of the view. If the devices still do not show up, go into the view template and Edit Overrides Annotations. Make sure that Sections and Section Boxes are checkmarked. Also make sure under Filters, the Visibility column is checkmarked for Sections - Not (Security/Comms).These steps should troubleshoot most of the possible problems you might have with the visibility of things.


:::